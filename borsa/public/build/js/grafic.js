var app = new Vue({
    el: '#appGrafic',
    data: {
        ofertes: [],
      daw:0,
      dam:0,
      asix:0,
      smix:0
    },
    mounted() {
        res = this;
        axios.get('http://localhost:8000/apiOfertes')
        .then(function (response) {
              res.ofertes = response.data;
              console.log(res.ofertes)
              for(let i=0; i<res.ofertes.length; i++){
                  if(res.ofertes[i].Categoria.nom=='DAW')
                      res.daw+=1;
                  else if(res.ofertes[i].Categoria.nom=='DAM')
                      res.dam+=1;
                  else if(res.ofertes[i].Categoria.nom=="SMIX")
                      res.smix+=1;
                  else
                      res.asix+=1;
              }
            var ctx = document.getElementById("myChart");
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: ['DAM', 'DAW', 'ASIX', 'SMIX'],
                    datasets: [{
                        label: 'Ofertes per categoria',
                        data: [res.dam,res.daw,res.asix,res.smix],
                        backgroundColor: [
                            '#7ecaf6',
                            '#f2849e',
                            '#ae85ca',
                            '#8499e7'
                        ],
                        borderColor: [
                            '#7ecaf6',
                            '#f2849e',
                            '#ae85ca',
                            '#8499e7'
                        ],
                        borderWidth: 1
                    }]
                }
            });
        });
    }
});