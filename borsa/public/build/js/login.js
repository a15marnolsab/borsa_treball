
$( document ).ready(function() {
    function checkIfLoggedIn()
    {
        if(sessionStorage.getItem('myUserEntity') == null){
            $("#googleSignOut").hide()
            $("#googleSignIn").show()
        } else {
            //User already logged in
            var userEntity = {};
            userEntity = JSON.parse(sessionStorage.getItem('myUserEntity'));
            $("#googleSignOut").show()
            $("#googleSignIn").hide()
        }
    }
    checkIfLoggedIn();
});
function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut();
    auth2.disconnect().then(function () {
        console.log('User signed out.');
        sessionStorage.clear();
        $("#googleSignOut").hide()
        $("#googleSignIn").show()
    });
}
function onLoadGoogleCallback(){
    gapi.load('auth2', function() {
        auth2 = gapi.auth2.init({
            client_id: '346943508129-epocq8e5orpblo07t17idriafcfh4h0i.apps.googleusercontent.com',
            cookiepolicy: 'single_host_origin',
            scope: 'profile',

        });

        auth2.attachClickHandler(element, {},
            function(googleUser) {
                var profile = googleUser.getBasicProfile();
                console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
                console.log('Name: ' + profile.getName());
                console.log('Image URL: ' + profile.getImageUrl());
                console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
                var myUserEntity = {};
                myUserEntity.Id = profile.getId();
                myUserEntity.Name = profile.getName();

                //Store the entity object in sessionStorage where it will be accessible from all pages of your site.
                sessionStorage.setItem('myUserEntity',JSON.stringify(myUserEntity));

                $("#googleSignOut").show()
                $("#googleSignIn").hide()
            }, function(error) {
                console.log('Sign-in error', error);
            }
        );
    });

    element = document.getElementById('googleSignIn');
}