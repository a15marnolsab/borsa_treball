var app = new Vue({
    el: '#app',
    data: {
      ofertes: [],
      ofertesCategoria: [],
      mostrar_oferta: false,
      ofertaActiu: {},
      carta: "Hem dirigeixo",
      dadesUsuari: {},
      ofertesCandidat:[]
    },
    mounted() {
        res = this;
        res.dadesUsuari = JSON.parse(sessionStorage.getItem('myUserEntity'));
        res.apiOfertes();
        res.apiOfertesCandidat();
        //console.log(res.dadesUsuari)
    },
    methods: {
        apiOfertesCandidat: function (){
            axios.get(`http://localhost:8000/apiOfertesCandidats/${res.dadesUsuari.Id}`)
                .then(function (response) {
                    res.ofertesCandidat = response.data;
                    //console.log(res.ofertesCandidat)
                    for(let i=0; i<res.ofertesCandidat.length;i++){
                        for(let j=0;j<res.ofertes.length;j++){
                            if(res.ofertesCandidat[i].oferta.id==res.ofertes[j].id){
                                res.ofertes[j].validacio=false;
                            }
                            res.ofertes[j].cv="";
                        }
                    }
                    //console.log(res.ofertes)
                })
        },
        apiOfertes: function(){
            axios.get('http://localhost:8000/apiOfertes')
                .then(function (response) {
                    res.ofertes = response.data;
                    res.ofertesCategoria= response.data;
                    for(let i=0; i<res.ofertes.length;i++) {
                        res.ofertes[i].destacat =res.addDays((res.ofertes[i].dataPublicacio.date), 15) > new Date();
                    }
                    //console.log(res.ofertes)
                })
        },
        ClassStyle: function (oferta) {
            if(oferta.Categoria.nom == "DAW")
                return "style1";
            else if (oferta.Categoria.nom == "DAM"){
                return "style2"
            }
            else if (oferta.Categoria.nom == "ASIX"){
                return "style5"
            }
            else{
                return "style6"
            }
        },
        MostrarInfoOferta: function (oferta) {
            this.ofertaActiu=oferta
            this.ofertaActiu.dataPublicacio=new Date(oferta.dataPublicacio.date).toLocaleDateString();
            this.mostrar_oferta=true
        },
        TancarOferta: function () {
            this.mostrar_oferta=false
        },
        MostrarCategoria: function (categoria) {
            if(categoria=="tots")
                this.ofertesCategoria=this.ofertes;
            else {
                this.ofertesCategoria = this.ofertes.filter(function (oferta) {
                    return oferta.Categoria.nom == categoria;
                })
            }
        },
        addDays: function (date, days) {
            var result = new Date(date);
            result.setDate(result.getDate() + days);
            return result;
        },
        inscriure: function () {
            if(res.ofertaActiu.cv) {
                axios.post('http://localhost:8000/apiOfertesPOST', {
                    idOfertes: res.ofertaActiu.id,
                    idUsuari: res.dadesUsuari.Id,
                    carta: res.carta
                })
                    .then(function (response) {
                        //console.log(response)
                        window.location.href = `http://localhost:8000/MostrarOfertes`;
                    })
                    .catch(function (error) {
                        //console.log(error);
                    });
            }
            else
                alert("Has d'aceptar l'autoritzacio de enviar CV.")
        }
    },
})
