var app = new Vue({
    el: '#appMenu',
    data: {
        logged: false,
        dadesUsuari: {},
    },
    mounted() {
        res=this;
        res.onLoadGoogleCallback();
        res.checkIfLoggedIn();
        res.dadesUsuari = JSON.parse(sessionStorage.getItem('myUserEntity'));
    },
    methods: {
        checkIfLoggedIn: function() {
            if(sessionStorage.getItem('myUserEntity') == null){
                res.logged=false
            } else {
                res.logged=true
            }
            res.onLoadGoogleCallback();
        },
        signOut: function () {
            let auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {
                //console.log('User signed out.');
                sessionStorage.clear();
                auth2.disconnect();
                res.checkIfLoggedIn();
            });
            auth2.disconnect();
            window.location.href = "http://localhost:8000";
        },
        onLoadGoogleCallback: function(){
            let element = document.getElementById('googleSignIn');
            gapi.load('auth2', function() {
                auth2 = gapi.auth2.init({
                    client_id: '346943508129-epocq8e5orpblo07t17idriafcfh4h0i.apps.googleusercontent.com',
                    cookiepolicy: 'single_host_origin',
                    scope: 'profile',

                });
                auth2.attachClickHandler(element, {},
                    function(googleUser) {
                        let profile = googleUser.getBasicProfile();
                        //console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
                        //console.log('Name: ' + profile.getName());
                        //console.log('Image URL: ' + profile.getImageUrl());
                        //console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
                        let myUserEntity = {};
                        myUserEntity.Id = profile.getId();
                        myUserEntity.Email = profile.getEmail();

                        //Store the entity object in sessionStorage where it will be accessible from all pages of your site.
                        sessionStorage.setItem('myUserEntity',JSON.stringify(myUserEntity));
                        window.location.href = `http://localhost:8000/inserirCandidat/${profile.getId()}/${profile.getName()}/${profile.getEmail()}`;
                    }, function(error) {
                        //console.log('Sign-in error', error);
                    }
                );
            });
        },
    },
});