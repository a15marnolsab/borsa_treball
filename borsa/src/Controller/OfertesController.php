<?php

namespace App\Controller;

use App\Entity\Candidat;
use App\Entity\CandidatOferta;
use App\Entity\Ofertes;
use App\Entity\Categoria;
use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Article;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class OfertesController extends AbstractController
{
    /**
     * @Route("/", name="portada")
     */
    public function  index(){
        return $this->render('base.html.twig');
    }
    /**
     * @Route("/MostrarOfertes", name="mostrarTots")
     */
    public function MostrarOfertes(){
        return $this->render('oferta/LlistarTots.html.twig');
    }
    /**
     * @Route("/afegiroferta", name="novaOferta")
     */
    public function AfegirOferta(Request $request)
    {
        $oferta = new Ofertes();
        $oferta->setDataPublicacio(new \DateTime());
        $oferta->setValidacio(false);
        $form = $this->createFormBuilder($oferta)
            ->add('titol', TextType::class)
            ->add('descripcio', TextareaType::class)
            ->add('Ubicacio', TextType::class)
            ->add('nom_empresa', TextType::class)
            ->add('link', TextType::class)
            ->add('email', TextType::class)
            ->add('Categoria', EntityType::class, array(
                'class' => 'App\Entity\Categoria' ,
                'choice_label' => 'nom'
            ))
            ->add('submit', SubmitType::class, ['label' => 'Enviar'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $oferta = $form->getData();
            $entityManager->persist($oferta);
            $entityManager->flush();
            return $this->render('oferta/successAdd.html.twig');
        }


        return $this->render('oferta/novaOferta.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/LlistarTots", name="Llistar_tots")
     */
    public function LlistarTots(){
        $ofertes = $this->getDoctrine()
            ->getRepository(Ofertes::class)
            ->findBy(array(), array('dataPublicacio' => 'DESC'));
        return $this->render('oferta/viewOfertes.html.twig', ['ofertes' => $ofertes]);
    }


    /**
     * @Route("/acceptar/{id}", name="acceptar")
     */
    public function acceptar($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $ofertes = $entityManager->getRepository(Ofertes::class)->find($id);

        $ofertes->setValidacio(true);
        $entityManager->flush();

        return $this->redirectToRoute('Llistar_tots');
    }
    /**
     * @Route("/descartar/{id}", name="descartar")
     */
    public function descartar($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $ofertes = $entityManager->getRepository(Ofertes::class)->find($id);

        $ofertes->setValidacio(false);
        $entityManager->flush();

        return $this->redirectToRoute('Llistar_tots');
    }

    /**
     * @Route("/eliminar/{id}", name="eliminar")
     */
    public function eliminar($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $ofertes = $entityManager->getRepository(Ofertes::class)->find($id);

        $entityManager->remove($ofertes);
        $entityManager->flush();

        return $this->redirectToRoute('Llistar_tots');
    }

    /**
     * @Route("/update/{id}", name="editar")
     */
    public function update(Request $request, $id)
    {
        $oferta = $this->getDoctrine()
            ->getRepository(Ofertes::class)
            ->find($id);

        $form = $this->createFormBuilder($oferta)
            ->add('titol', TextType::class)
            ->add('descripcio', TextareaType::class)
            ->add('Ubicacio', TextType::class)
            ->add('nom_empresa', TextType::class)
            ->add('link', TextType::class)
            ->add('email', TextType::class)
            ->add('Categoria', EntityType::class, array(
                'class' => 'App\Entity\Categoria' ,
                'choice_label' => 'nom'
            ))
            ->add('submit', SubmitType::class, ['label' => 'Editar'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $oferta = $form->getData();
            $entityManager->persist($oferta);
            $entityManager->flush();
            return $this->redirectToRoute('Llistar_tots');
        }


        return $this->render('oferta/novaOferta.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/apiOfertes", name="api", methods={"GET","HEAD"})
     */
    public function apiOfertes(Request $request){
        $date = new \DateTime();
        $date->modify('-90 days');
        $ofertes = $this->getDoctrine()
            ->getRepository(Ofertes::class)
            ->createQueryBuilder('ofertes')
            ->innerJoin('ofertes.Categoria', 'Categoria')
            ->addSelect('Categoria')
            ->andWhere('ofertes.categoria_id = :id')
            ->where('ofertes.validacio = 1')
            ->andWhere('ofertes.dataPublicacio > :date')
            ->setParameter(':date', $date)
            ->addOrderBy('ofertes.dataPublicacio', 'DESC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return new JsonResponse($ofertes);
    }


    /**
     * @Route("/apiOfertesPOST", name="apiPOST", methods={"POST","HEAD"})
     */
    public function apiOfertesPost(Request $request){

        $data = json_decode($request->getContent(), true);

        $found = $this->getDoctrine()
            ->getRepository(Candidat::class)
            ->createQueryBuilder('Candidat')
            ->addSelect('Candidat')
            ->andWhere('Candidat.usuariID = :id')
            ->setParameter('id', $data['idUsuari'])
            ->getQuery();

        $candidat=$found->getResult();

        $found = $this->getDoctrine()
            ->getRepository(Ofertes::class)
            ->createQueryBuilder('ofertes')
            ->addSelect('ofertes')
            ->andWhere('ofertes.id = :id')
            ->setParameter('id', $data['idOfertes'])
            ->getQuery();

        $oferta = $found->getResult();

        $candidatOferta = new CandidatOferta();
        $candidatOferta->setHora(new \DateTime("now"));
        $candidatOferta->setOferta($oferta[0]);
        $candidatOferta->setUsuari($candidat[0]);
        $candidatOferta->setCartaPres($data['carta']);

        $entityManager= $this->getDoctrine()->getManager();
        $entityManager->persist($candidatOferta);
        $entityManager->flush();

        return $this->redirectToRoute('EnviarMail',["id"=>$candidat[0]->getId(),"remitente"=>$oferta[0]->getEmail(),"mensaje"=>$data['carta']]);
    }
}
