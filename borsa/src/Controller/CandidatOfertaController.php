<?php

namespace App\Controller;

use App\Entity\CandidatOferta;
use App\Entity\Candidat;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;



class CandidatOfertaController extends AbstractController
{
    /**
     * @Route("/LlistarCandidats/{id}", name="candidatOferta")
     */
    public function llistarCandidats($id)
    {
        $candidats = $this->getDoctrine()
            ->getRepository(CandidatOferta::class)
            ->findBy(['oferta' => $id]);

        $inscripcions = $this->getDoctrine()
            ->getRepository(CandidatOferta::class)
            ->count(['oferta' => $id]
        );
        return $this->render('candidat/llistat.html.twig', ['candidats' => $candidats, 'inscripcions' => $inscripcions]);
    }

    /**
     * @Route("/apiOfertesCandidats/{id}", name="apiOfertesCandidats", methods={"GET","HEAD"})
     */
    public function apiOfertesCandidats(Request $request, $id){
        $candidat = $this->getDoctrine()
            ->getRepository(Candidat::class)
            ->findBy(['usuariID' => $id]);

        $ofertasCandidats = $this->getDoctrine()
            ->getRepository(CandidatOferta::class)
            ->createQueryBuilder('Candidat')
            ->innerJoin('Candidat.oferta', 'oferta')
            ->innerJoin('Candidat.usuari', 'usuari')
            ->addSelect('oferta, usuari')
            ->andWhere('Candidat.usuari = :id')
            ->setParameter('id', $candidat[0]->getId() )
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return new JsonResponse($ofertasCandidats);
    }

}

