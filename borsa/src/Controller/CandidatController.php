<?php

namespace App\Controller;

use App\Entity\Candidat;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Article;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle;


class CandidatController extends AbstractController
{
    /**
     * @Route("/candidat", name="candidat")
     */
    public function index()
    {
        return $this->render('candidat/index.html.twig', [
            'controller_name' => 'CandidatController',
        ]);
    }
    /**
     * @Route("/inserirCandidat/{id}/{usuari}/{email}", name="inserirCandidat")
     */
    public function inserirCandidat($id, $usuari,$email)
    {

        $candidat = $this->getDoctrine()
        ->getRepository(Candidat::class)
        ->findBy(array('usuariID' =>  $id));

        if (!$candidat) {

            $candidat = new Candidat();
            $candidat->setusuariID($id);
            $candidat->setUsuari($usuari);
            $candidat->setEmail($email);
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($candidat);

            $entityManager->flush();

            return $this->redirectToRoute('portada');
        }

        return $this->redirectToRoute('mostrarTots');
    

    }

    /**
     * @Route("/pujarCv/{id}", name="pujarCv")
     */
    public function Pujarcv(Request $request, $id)
    {

        $candidat = $this->getDoctrine()
            ->getRepository(Candidat::class)
            ->findBy(['usuariID' => $id]);

        $form = $this->createFormBuilder()
            ->add('curriculum', FileType::class,array(
                "label" => "Curriculum:",
                "attr" =>array("class" => "form-control")
            ))
            ->add('submit', SubmitType::class, ['label' => 'Pujar'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $file=$form['curriculum']->getData();
            $ext=$file->guessExtension();
            $file_name=time().".".$ext;
            $file->move("cv/", $file_name);
            $candidat[0]->setCurriculum($file_name);
            $entityManager->persist($candidat[0]);
            $entityManager->flush();
            return $this->redirectToRoute('portada');
        }

        return $this->render('candidat/curriculum.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/EnviarMail/{id}/{remitente}/{mensaje}", name="EnviarMail")
     */
    public function enviarMailcv(\Swift_Mailer $mailer, $id, $remitente, $mensaje){

        $entityManager = $this->getDoctrine()->getManager();
        $emisor = $entityManager->getRepository(Candidat::class)->find($id);

        $cv = $emisor->getCurriculum();

        $message = (new \Swift_Message('Un usuario se ha inscrito a tu oferta!'))
            ->setFrom("mabeeh.6@gmail.com")
            ->setTo($remitente)
            ->setSubject('Subject')
            ->setBody($mensaje)
            ->attach(\Swift_Attachment::fromPath('cv/'.$cv));

        try{
            $mailer->send($message);
        }catch(\Swift_TransportException $e){
            $response = $e->getMessage();
        }

        return $this->redirectToRoute('mostrarTots');
    }
}

