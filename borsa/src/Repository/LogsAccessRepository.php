<?php

namespace App\Repository;

use App\Entity\LogsAccess;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LogsAccess|null find($id, $lockMode = null, $lockVersion = null)
 * @method LogsAccess|null findOneBy(array $criteria, array $orderBy = null)
 * @method LogsAccess[]    findAll()
 * @method LogsAccess[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogsAccessRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LogsAccess::class);
    }

    // /**
    //  * @return LogsAccess[] Returns an array of LogsAccess objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LogsAccess
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
