<?php

namespace App\Repository;

use App\Entity\Ofertes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Ofertes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ofertes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ofertes[]    findAll()
 * @method Ofertes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfertesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ofertes::class);
    }

    // /**
    //  * @return Ofertes[] Returns an array of Ofertes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ofertes
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
