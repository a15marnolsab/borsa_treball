<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LogsAccessRepository")
 */
class LogsAccess
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="datetime")
     */
    private $hora;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Candidat", inversedBy="usuariLog")
     */
    private $usuari;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $ip;

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getHora(): ?\DateTimeInterface
    {
        return $this->hora;
    }

    public function setHora(\DateTimeInterface $hora): self
    {
        $this->hora = $hora;

        return $this;
    }

    public function getUsuari(): ?Candidat
    {
        return $this->usuari;
    }

    public function setUsuari(?Candidat $usuari): self
    {
        $this->usuari = $usuari;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }
}
