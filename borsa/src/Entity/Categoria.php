<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoriaRepository")
 */
class Categoria
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
    */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ofertes", mappedBy="Categoria")
     */
    private $ofertes;

    public function __construct()
    {
        $this->ofertes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setDescripcio(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|Ofertes[]
     */
    public function getOfertes(): Collection
    {
        return $this->ofertes;
    }

    public function addOferte(Ofertes $oferte): self
    {
        if (!$this->ofertes->contains($oferte)) {
            $this->ofertes[] = $oferte;
            $oferte->setCategoria($this);
        }

        return $this;
    }

    public function removeOferte(Ofertes $oferte): self
    {
        if ($this->ofertes->contains($oferte)) {
            $this->ofertes->removeElement($oferte);
            // set the owning side to null (unless already changed)
            if ($oferte->getCategoria() === $this) {
                $oferte->setCategoria(null);
            }
        }

        return $this;
    }
}
