<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OfertesRepository")
 */
class Ofertes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $titol;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descripcio;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dataPublicacio;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $ubicacio;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nomEmpresa;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $link;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria", inversedBy="ofertes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Categoria;

    /**
     * @ORM\Column(type="boolean")
     */
    private $validacio;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CandidatOferta", mappedBy="oferta", orphanRemoval=true)
     */
    private $candidatOfertas;

    public function __construct()
    {
        $this->candidatOfertas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitol(): ?string
    {
        return $this->titol;
    }

    public function setTitol(string $titol): self
    {
        $this->titol = $titol;

        return $this;
    }

    public function getDescripcio(): ?string
    {
        return $this->descripcio;
    }

    public function setDescripcio(string $descripcio): self
    {
        $this->descripcio = $descripcio;

        return $this;
    }

    public function getDataPublicacio(): ?\DateTimeInterface
    {
        return $this->dataPublicacio;
    }

    public function setDataPublicacio(?\DateTimeInterface $dataPublicacio): self
    {
        $this->dataPublicacio = $dataPublicacio;

        return $this;
    }

    public function getUbicacio(): ?string
    {
        return $this->ubicacio;
    }

    public function setUbicacio(string $ubicacio): self
    {
        $this->ubicacio = $ubicacio;

        return $this;
    }

    public function getNomEmpresa(): ?string
    {
        return $this->nomEmpresa;
    }

    public function setNomEmpresa(string $nomEmpresa): self
    {
        $this->nomEmpresa = $nomEmpresa;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCategoria(): ?Categoria
    {
        return $this->Categoria;
    }

    public function setCategoria(?Categoria $Categoria): self
    {
        $this->Categoria = $Categoria;

        return $this;
    }

    public function getValidacio(): ?bool
    {
        return $this->validacio;
    }

    public function setValidacio(bool $validacio): self
    {
        $this->validacio = $validacio;

        return $this;
    }

    /**
     * @return Collection|CandidatOferta[]
     */
    public function getCandidatOfertas(): Collection
    {
        return $this->candidatOfertas;
    }

    public function addCandidatOferta(CandidatOferta $candidatOferta): self
    {
        if (!$this->candidatOfertas->contains($candidatOferta)) {
            $this->candidatOfertas[] = $candidatOferta;
            $candidatOferta->setOferta($this);
        }

        return $this;
    }

    public function removeCandidatOferta(CandidatOferta $candidatOferta): self
    {
        if ($this->candidatOfertas->contains($candidatOferta)) {
            $this->candidatOfertas->removeElement($candidatOferta);
            // set the owning side to null (unless already changed)
            if ($candidatOferta->getOferta() === $this) {
                $candidatOferta->setOferta(null);
            }
        }

        return $this;
    }
}
