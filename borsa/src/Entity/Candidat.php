<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CandidatRepository")
 */
class Candidat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20, unique=true)
     */
    private $usuari;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $curriculum;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LogsAccess", mappedBy="usuari")
     */
    private $usuariLog;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $usuariID;


    public function __construct()
    {
        $this->usuariLog = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsuari(): ?string
    {
        return $this->usuari;
    }

    public function setUsuari(string $usuari): self
    {
        $this->usuari = $usuari;

        return $this;
    }

    public function getCurriculum(): ?string
    {
        return $this->curriculum;
    }

    public function setCurriculum(?string $curriculum): self
    {
        $this->curriculum = $curriculum;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|LogsAccess[]
     */
    public function getUsuariLog(): Collection
    {
        return $this->usuariLog;
    }

    public function addUsuariLog(LogsAccess $usuariLog): self
    {
        if (!$this->usuariLog->contains($usuariLog)) {
            $this->usuariLog[] = $usuariLog;
            $usuariLog->setUsuari($this);
        }

        return $this;
    }

    public function removeUsuariLog(LogsAccess $usuariLog): self
    {
        if ($this->usuariLog->contains($usuariLog)) {
            $this->usuariLog->removeElement($usuariLog);
            // set the owning side to null (unless already changed)
            if ($usuariLog->getUsuari() === $this) {
                $usuariLog->setUsuari(null);
            }
        }

        return $this;
    }

    public function getusuariID(): ?string
    {
        return $this->usuariID;
    }

    public function setusuariID(string $usuariID): self
    {
        $this->usuariID = $usuariID;

        return $this;
    }
}
