<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CandidatOfertaRepository")
 */
class CandidatOferta
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Candidat", inversedBy="idUsuari")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $usuari;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ofertes", inversedBy="candidatOfertas")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $oferta;

    /**
     * @ORM\Column(type="datetime")
     */
    private $hora;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $carta_pres;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsuari(): ?Candidat
    {
        return $this->usuari;
    }

    public function setUsuari(?Candidat $usuari): self
    {
        $this->usuari = $usuari;

        return $this;
    }

    public function getOferta(): ?Ofertes
    {
        return $this->oferta;
    }

    public function setOferta(?Ofertes $oferta): self
    {
        $this->oferta = $oferta;

        return $this;
    }

    public function getHora(): ?\DateTimeInterface
    {
        return $this->hora;
    }

    public function setHora(\DateTimeInterface $hora): self
    {
        $this->hora = $hora;

        return $this;
    }

    public function getCartaPres(): ?string
    {
        return $this->carta_pres;
    }

    public function setCartaPres(?string $carta_pres): self
    {
        $this->carta_pres = $carta_pres;

        return $this;
    }

}
